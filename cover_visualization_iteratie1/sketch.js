var song;
var amp;
var button;

var volhistory = [];

function toggleSong() {
  if (song.isPlaying()) {
    song.pause();
  } else {
    song.play();
  }
}

function preload() {
  song = loadSound('breathin.wav');
}

function setup() {
  createCanvas(960, 540);
  button = createButton('toggle');
  button.mousePressed(toggleSong);
  song.play();
  amp = new p5.Amplitude();
}

function draw() {
  background(0, 177, 64);
  var vol = amp.getLevel();
  volhistory.push(vol);
  stroke(255);
  strokeWeight(3);
  strokeCap(ROUND);
  noFill();
  beginShape();
  for (var i = 0; i < volhistory.length; i++) {
    var y = map(volhistory[i], 0, 1, height, 300);
    // point(i, y);
    line(i, height+20, i, y);
  }
  endShape();
  
  if (volhistory.length > width - 2*width/5) {
    volhistory.splice(0, 1);
  }

  stroke(255);
  strokeWeight(4);
  strokeCap(ROUND);
  line(volhistory.length, height, volhistory.length, 420);
  line(volhistory.length+1, height, volhistory.length+1, 420);
  line(volhistory.length+2, height, volhistory.length+2, 420);
  line(volhistory.length+3, height, volhistory.length+3, 420);
}